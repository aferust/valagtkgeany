
namespace Valagtkgeany {
	string slash;
	
	string gtkver;
	string giover;
	
	public class Main : Object {
		private static bool version = false;
		private static string? directory = null;
		private static string? name = null;
		private static string? _gtkver = null;
		private static string? _giover = null;

		public static int main (string[] args) {
			slash = Path.DIR_SEPARATOR_S;
			GLib.File file = GLib.File.new_for_path(Environment.get_home_dir());
			
			const GLib.OptionEntry[] options = {
				// --version
				{ "version", 'v', 0, OptionArg.NONE, ref version, "Display version number", null },

				// --directory FIlENAME || -o FILENAME
				{ "directory", 'o', 0, OptionArg.FILENAME, ref directory, "root directory of the project", "~/" },

				// --name
				{ "name", 'n', 0, OptionArg.STRING, ref name, "set name of the project", "helloworld" },
				
				// --giover
				{ "giover", 'i', 0, OptionArg.STRING, ref _giover, "set minimum gio version", "2.48.2" },
				
				// --gtkver
				{ "gtkver", 'k', 0, OptionArg.STRING, ref _gtkver, "set minimum gtk+ version", "3.20" },
				
				// list terminator
				{ null }
			};
			
			try {
				var opt_context = new OptionContext ("- Usage");
				opt_context.set_help_enabled (true);
				opt_context.add_main_entries (options, null);
				opt_context.parse (ref args);
			} catch (OptionError e) {
				print ("error: %s\n", e.message);
				print ("Run '%s --help' to see a full list of available command line options.\n", args[0]);
				return 0;
			}

			if (version) {
				print ("0.1\n");
				return 0;
			}
			
			// set default parameters
			if(directory == null) directory = file.get_path();
			if(name == null) name = "helloworld";
			if(_giover == null) _giover = "2.48.2";
			if(_gtkver == null) _gtkver = "3.20";
			
			print (@" directory: '$directory'\n");
			print (@" name: '$name'\n");
			print (@" gio version: '$_giover'\n");
			print (@" gtk+ version: '$_gtkver'\n");
			
			gtkver = _gtkver;
			giover = _giover;
			
			GLib.File fl;
			if(is_valid_file_name(name.strip())){
				try {
					fl = GLib.File.new_for_path(directory + slash + name.strip());
					fl.make_directory ();
					append_str("\nBase folder created.");
					
					Creator worker = new Creator(fl.get_path(), name.strip());
					
					worker.make_root_folder();
					append_str(worker.make_src_folder());
					append_str(worker.make_po_folder());
					append_str(worker.make_data_folder());
					append_str(worker.make_build_aux_folder());
					append_str(worker.make_build_folder());
					append_str(worker.make_win_res_folder());
					
				} catch (Error e) {
					append_str("\n" + e.message);
				}					
			}else{
				append_str("\nPlease provide a valid file name!");
			}

			return 0;
		}
		
		public static void append_str(string str){
			 stdout.printf(str);
		}
		
		public static bool is_valid_file_name(string str){
			if(str == "") return false;
			if(str == null) return false;
			for (int i = 0; i < str.char_count (); i++) {
				string chr = str.get_char (str.index_of_nth_char (i)).to_string ();
				if(Uri.RESERVED_CHARS_ALLOWED_IN_PATH.index_of(chr) != -1) return false;
				if(chr == " ") return false;
			}
			return true;
		}
	}
}
