/* creator.vala
 *
 * Copyright 2018 Ferhat Kurtulmuş <aferust@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

extern int is_win();

namespace Valagtkgeany {
	class Creator: Object {
		
		public bool workingProperty { set; get; }
		
		public string path;
		public string name;
		public string Name;
		
		public Creator (string path, string name) {
			this.path = path;
			this.name = name;
			
			string first = name.substring(0, 1).up();
			string rest = name.substring(1, -1);
			this.Name = first + rest;
			
			//stdout.printf(this.Name);
		}
		
		public void make_win_res_icon(){
			Gdk.Pixbuf image = null;
			try {
				image = new Gdk.Pixbuf.from_resource("/org/gnome/Valagtkgeany/../win_res/app.ico");
				image.savev(path + slash + "win_res" + slash + "app.ico", "ico", {"quality"}, {"100"});
			} catch (GLib.Error ex) {
				stdout.printf(ex.message);
			}
			
		}
		
		public void make_win_res_resource_rc(){
			try{
				var file = File.new_for_path (path + slash + "win_res" + slash + "resource.rc");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string ("1 ICON app.ico");
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public string make_win_res_folder(){
			GLib.File fl = GLib.File.new_for_path(path + slash + "win_res");
			try{
				fl.make_directory ();
				make_win_res_icon();
				make_win_res_resource_rc();
				
				return "\nwin_res folder created.";
			} catch (Error e) {
				stdout.printf("\n" + e.message);
				return "\n"+e.message;
			}
		}
		
		public void make_root_folder(){
			make_root_meson_file();
			make_root_json_file();
			make_root_copying_file();
			make_root_geany_file();
		}
		
		public string make_po_folder(){
			GLib.File fl = GLib.File.new_for_path(path + slash + "po");
			try{
				fl.make_directory ();
				make_po_LINGUAS();
				make_po_POTFILES();
				make_po_meson();
				
				return "\npo folder created.";
			} catch (Error e) {
				stdout.printf("\n" + e.message);
				return "\n"+e.message;
			}		
		}
		
		public void make_po_LINGUAS(){
			try{
				var file = File.new_for_path (path + slash + "po" + slash + "LINGUAS");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (" ");
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}	
		}
		
		public void make_po_POTFILES(){
			try{
				var file = File.new_for_path (path + slash + "po" + slash + "POTFILES");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_po_POTFILES_file(Name));
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}		
		}
		
		public void make_po_meson(){
			try{
				var file = File.new_for_path (path + slash + "po" + slash + "meson.build");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (@"i18n.gettext('$name', preset: 'glib')\r");
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public string make_data_folder(){
			try{
				GLib.File fl = GLib.File.new_for_path(path + slash + "data");
				fl.make_directory ();
					
				make_data_meson_file();
				make_data_appdata_xml_in_file();
				make_data_desktop_in_file();
				make_data_gschema_xml_file();
				
				return "\ndata folder created.";
			} catch (Error e) {
				return "\n" + e.message;
			}
		}
		
		public void make_data_meson_file(){
			try{
				var file = File.new_for_path (path + slash + "data" + slash + "meson.build");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_data_meson_file(Name));
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}		
		}
		
		public void make_data_appdata_xml_in_file(){
			try{
				var file = File.new_for_path (path + slash + "data" + slash + "org.gnome." + Name + ".appdata.xml.in");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_data_appdata_xml_in_file().printf(Name));
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public void make_data_gschema_xml_file(){
			try{
				var file = File.new_for_path (path + slash + "data" + slash + "org.gnome." + Name + ".gschema.xml");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_data_gschema_xml_file().printf(name, Name, Name));
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}		
		}
		
		public void make_data_desktop_in_file(){
			try{
				var file = File.new_for_path (path + slash + "data" + slash + "org.gnome." + Name + ".desktop.in");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_data_desktop_in_file(name));
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public string make_src_folder () {
			try {
				GLib.File fl = GLib.File.new_for_path(path + slash + "src");
				fl.make_directory ();
				
				make_main_file();
				make_controller_file();
				make_glade_file();
				make_src_meson_file();
				make_src_gresource_file();
				
				return "\nsrc folder created.";
				
			} catch (Error e) {
				return "\n" + e.message;
			}
			
		}
		
		public void make_main_file(){
			try{
				var file = File.new_for_path (path + slash + "src" + slash + "main.vala");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_main_template().printf(Name, Name));
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public void make_controller_file(){
			try{
				var file = File.new_for_path (path + slash + "src" + slash + "window.vala");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_controler_template().printf(Name, Name, Name));
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public void make_glade_file(){
			try{
				var file = File.new_for_path (path + slash + "src" + slash + "window.ui");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_glade_template().printf(Name));
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public void make_src_meson_file(){
			try{
				var file = File.new_for_path (path + slash + "src" + slash + "meson.build");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_src_meson_file().printf(name, name, giover, gtkver, name, name, name, name, name, name, name, giover, name));
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public void make_src_gresource_file(){
			try{
				var file = File.new_for_path (path + slash + "src" + slash + name + ".gresource.xml");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_src_gresource_file().printf(Name, Name));		
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public void make_root_meson_file(){
			var file = File.new_for_path (path + slash + "meson.build");
			try {
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				
				data_stream.put_string (Templates.get_root_meson_file().printf(name));
				
			} catch (Error e) {
				stdout.printf("\n" + e.message);
			}
			
			
		}
		
		public void make_root_json_file(){
			try{
				var file = File.new_for_path (path + slash + "org.gnome." + Name + ".json");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_root_json_file().printf(Name, name, name, name));
				
            } catch (Error e) {
				stdout.printf( "\n" + e.message );
			}	
		}
		
		public void make_root_copying_file(){
			try{
				var file = File.new_for_path (path + slash + "COPYING");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_root_copying_file());
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}		
		}
		
		public void make_root_geany_file(){
			try{
				var file = File.new_for_path (path + slash + Name + ".geany");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_root_geany_file(name, path));
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public string make_build_aux_folder(){
			try{
				GLib.File fl = GLib.File.new_for_path(path + slash + "build-aux");
				fl.make_directory ();
				
				GLib.File fl2 = GLib.File.new_for_path(path + slash + "build-aux" + slash + "meson");
				fl2.make_directory ();
				
				make_build_aux_postinstall_file();
				
				return "\nbuild-aux folder created.";
			} catch (Error e) {
				return "\n" + e.message;
			}
			
		}
		
		public void make_build_aux_postinstall_file(){
			try{
				var file = File.new_for_path (path + slash + "build-aux" + slash + "meson" + slash + "postinstall.py");
				var file_stream = file.create (FileCreateFlags.NONE);
				var data_stream = new DataOutputStream (file_stream);
				data_stream.put_string (Templates.get_build_aux_postinstall_file());			
			} catch (Error e) {
				stdout.printf( "\n" + e.message );
			}
		}
		
		public string make_build_folder(){
			try{
				GLib.File fl = GLib.File.new_for_path(path + slash + "build");
				fl.make_directory ();
				
				return "\nbuild folder created.";
			} catch (Error e) {
				return "\n" + e.message;
			}
		}
	}
}
