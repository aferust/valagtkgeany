/* templates.vala
 *
 * Copyright 2018 Ferhat Kurtulmuş <aferust@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Valagtkgeany {
	public class Templates: Object{
		public static string get_main_template(){	
			return """/* main.vala
 *
 * Copyright 2018 user
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

int main (string[] args) {
	var app = new Gtk.Application ("org.gnome.%s", ApplicationFlags.FLAGS_NONE);
	app.activate.connect (() => {
		var win = app.active_window;
		if (win == null) {
			win = new %s.Window (app);
		}
		
		win.present ();
	});
	
	return app.run (args);
}
""";
		}
		public static string get_controler_template(){
			return """/* window.vala
 *
 * Copyright 2018 user
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace %s {
	string slash;
	
	[GtkTemplate (ui = "/org/gnome/%s/window.ui")]
	public class Window : Gtk.ApplicationWindow {
		
		[GtkChild]
		Gtk.Label label1;
		
		[GtkChild]
		Gtk.Button but1;
		
		public Window (Gtk.Application app) {
			Object (application: app);
			
			this.icon = new Gdk.Pixbuf.from_resource("/org/gnome/%s/../win_res/app.ico");
			
			slash = Path.DIR_SEPARATOR_S;
			
			label1.set_text("Hello World");
			
			but1.clicked.connect (() => {
				stdout.printf("Hello World");
			});
			
		}
		
	}
}
""";
		}
		public static string get_glade_template(){
			return """<?xml version="1.0" encoding="UTF-8"?>
<!-- Generated with glade 3.20.3 -->
<interface>
  <requires lib="gtk+" version="3.20"/>
  <template class="%sWindow" parent="GtkApplicationWindow">
    <property name="can_focus">False</property>
    <child>
      <object class="GtkBox">
        <property name="visible">True</property>
        <property name="can_focus">False</property>
        <property name="orientation">vertical</property>
        <property name="spacing">10</property>
        <child>
          <object class="GtkLabel" id="label1">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="label" translatable="yes">label</property>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">0</property>
          </packing>
        </child>
        <child>
          <object class="GtkButton" id="but1">
            <property name="label" translatable="yes">button</property>
            <property name="visible">True</property>
            <property name="can_focus">True</property>
            <property name="receives_default">True</property>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">1</property>
          </packing>
        </child>
      </object>
    </child>
    <child>
      <placeholder/>
    </child>
  </template>
</interface>
""";
		}
		public static string get_src_meson_file(){
			return """%s_sources = [
  'main.vala',
  'window.vala',
]

%s_deps = [
  dependency('gio-2.0', version: '>= %s'),
  dependency('gtk+-3.0', version: '>= %s'),
]

gnome = import('gnome')

%s_sources += gnome.compile_resources('%s-resources',
  '%s.gresource.xml',
  c_name: '%s'
)

if host_machine.system() == 'windows'
	windows = import('windows')
	%s_sources += windows.compile_resources('../win_res/resource.rc')
endif

executable('%s', %s_sources,
  vala_args: '--target-glib=%s',  dependencies: %s_deps,
  install: true,
  #gui_app : true,
)
""";
		}
		
		public static string get_src_gresource_file(){
			return """<?xml version="1.0" encoding="UTF-8"?>
<gresources>
  <gresource prefix="/org/gnome/%s">
    <file>window.ui</file>
  </gresource>
  <gresource prefix="/org/gnome/%s/">
    <file>../win_res/app.ico</file>
  </gresource>
</gresources>
""";
		}
		
		public static string get_root_meson_file(){
			return """project('%s', ['c', 'vala', 'cpp'],
  version: '0.1.0',
  meson_version: '>= 0.40.0',
  default_options : ['cpp_std=c++11','b_pch=false'],
)

i18n = import('i18n')

if host_machine.system() != 'windows'
	subdir('data')
endif

subdir('src')
subdir('po')

meson.add_install_script('build-aux/meson/postinstall.py')			
""";
		}
		
		public static string get_root_json_file(){
			return """{
    "app-id": "org.gnome.%s",
    "runtime": "org.gnome.Platform",
    "runtime-version": "3.28",
    "sdk": "org.gnome.Sdk",
    "command": "%s",
    "finish-args": [
        "--share=network",
        "--share=ipc",
        "--socket=x11",
        "--socket=wayland",
        "--filesystem=xdg-run/dconf",
        "--filesystem=~/.config/dconf:ro",
        "--talk-name=ca.desrt.dconf",
        "--env=DCONF_USER_CONFIG_DIR=.config/dconf"
    ],
    "build-options": {
        "cflags": "-O2 -g",
        "cxxflags": "-O2 -g",
        "env": {
            "V": "1"
        }
    },
    "cleanup": [
        "/include",
        "/lib/pkgconfig",
        "/man",
        "/share/doc",
        "/share/gtk-doc",
        "/share/man",
        "/share/pkgconfig",
        "/share/vala",
        "*.la",
        "*.a"
    ],
    "modules": [
        {
            "name": "%s",
            "buildsystem": "meson",
            "config-opts": [ "--libdir=lib" ],
            "builddir": true,
            "sources": [
                {
                    "type": "git",
                    "url": "file:///home/user/Projects/%s"
                }
            ]
        }
    ]
}
""";
		}
		
		public static string get_root_copying_file(){
			return """                   GNU LESSER GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


  This version of the GNU Lesser General Public License incorporates
the terms and conditions of version 3 of the GNU General Public
License, supplemented by the additional permissions listed below.

  0. Additional Definitions.

  As used herein, "this License" refers to version 3 of the GNU Lesser
General Public License, and the "GNU GPL" refers to version 3 of the GNU
General Public License.

  "The Library" refers to a covered work governed by this License,
other than an Application or a Combined Work as defined below.

  An "Application" is any work that makes use of an interface provided
by the Library, but which is not otherwise based on the Library.
Defining a subclass of a class defined by the Library is deemed a mode
of using an interface provided by the Library.

  A "Combined Work" is a work produced by combining or linking an
Application with the Library.  The particular version of the Library
with which the Combined Work was made is also called the "Linked
Version".

  The "Minimal Corresponding Source" for a Combined Work means the
Corresponding Source for the Combined Work, excluding any source code
for portions of the Combined Work that, considered in isolation, are
based on the Application, and not on the Linked Version.

  The "Corresponding Application Code" for a Combined Work means the
object code and/or source code for the Application, including any data
and utility programs needed for reproducing the Combined Work from the
Application, but excluding the System Libraries of the Combined Work.

  1. Exception to Section 3 of the GNU GPL.

  You may convey a covered work under sections 3 and 4 of this License
without being bound by section 3 of the GNU GPL.

  2. Conveying Modified Versions.

  If you modify a copy of the Library, and, in your modifications, a
facility refers to a function or data to be supplied by an Application
that uses the facility (other than as an argument passed when the
facility is invoked), then you may convey a copy of the modified
version:

   a) under this License, provided that you make a good faith effort to
   ensure that, in the event an Application does not supply the
   function or data, the facility still operates, and performs
   whatever part of its purpose remains meaningful, or

   b) under the GNU GPL, with none of the additional permissions of
   this License applicable to that copy.

  3. Object Code Incorporating Material from Library Header Files.

  The object code form of an Application may incorporate material from
a header file that is part of the Library.  You may convey such object
code under terms of your choice, provided that, if the incorporated
material is not limited to numerical parameters, data structure
layouts and accessors, or small macros, inline functions and templates
(ten or fewer lines in length), you do both of the following:

   a) Give prominent notice with each copy of the object code that the
   Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the object code with a copy of the GNU GPL and this license
   document.

  4. Combined Works.

  You may convey a Combined Work under terms of your choice that,
taken together, effectively do not restrict modification of the
portions of the Library contained in the Combined Work and reverse
engineering for debugging such modifications, if you also do each of
the following:

   a) Give prominent notice with each copy of the Combined Work that
   the Library is used in it and that the Library and its use are
   covered by this License.

   b) Accompany the Combined Work with a copy of the GNU GPL and this license
   document.

   c) For a Combined Work that displays copyright notices during
   execution, include the copyright notice for the Library among
   these notices, as well as a reference directing the user to the
   copies of the GNU GPL and this license document.

   d) Do one of the following:

       0) Convey the Minimal Corresponding Source under the terms of this
       License, and the Corresponding Application Code in a form
       suitable for, and under terms that permit, the user to
       recombine or relink the Application with a modified version of
       the Linked Version to produce a modified Combined Work, in the
       manner specified by section 6 of the GNU GPL for conveying
       Corresponding Source.

       1) Use a suitable shared library mechanism for linking with the
       Library.  A suitable mechanism is one that (a) uses at run time
       a copy of the Library already present on the user's computer
       system, and (b) will operate properly with a modified version
       of the Library that is interface-compatible with the Linked
       Version.

   e) Provide Installation Information, but only if you would otherwise
   be required to provide such information under section 6 of the
   GNU GPL, and only to the extent that such information is
   necessary to install and execute a modified version of the
   Combined Work produced by recombining or relinking the
   Application with a modified version of the Linked Version. (If
   you use option 4d0, the Installation Information must accompany
   the Minimal Corresponding Source and Corresponding Application
   Code. If you use option 4d1, you must provide the Installation
   Information in the manner specified by section 6 of the GNU GPL
   for conveying Corresponding Source.)

  5. Combined Libraries.

  You may place library facilities that are a work based on the
Library side by side in a single library together with other library
facilities that are not Applications and are not covered by this
License, and convey such a combined library under terms of your
choice, if you do both of the following:

   a) Accompany the combined library with a copy of the same work based
   on the Library, uncombined with any other library facilities,
   conveyed under the terms of this License.

   b) Give prominent notice with the combined library that part of it
   is a work based on the Library, and explaining where to find the
   accompanying uncombined form of the same work.

  6. Revised Versions of the GNU Lesser General Public License.

  The Free Software Foundation may publish revised and/or new versions
of the GNU Lesser General Public License from time to time. Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

  Each version is given a distinguishing version number. If the
Library as you received it specifies that a certain numbered version
of the GNU Lesser General Public License "or any later version"
applies to it, you have the option of following the terms and
conditions either of that published version or of any later version
published by the Free Software Foundation. If the Library as you
received it does not specify a version number of the GNU Lesser
General Public License, you may choose any version of the GNU Lesser
General Public License ever published by the Free Software Foundation.

  If the Library as you received it specifies that a proxy can decide
whether future versions of the GNU Lesser General Public License shall
apply, that proxy's public statement of acceptance of any version is
permanent authorization for you to choose that version for the
Library.
""";
		}
		
		public static string get_root_geany_file(string name, string _path){
			string sl = slash;
			string path = _path;
			
			if(is_win()==1){
				sl = slash + slash;
				path = path.replace ("\\", "\\\\");
			}
			
			string EX_00_CM = "%p" + sl + "build" + sl + "src" + sl + name;
			string ValaFT_00_WD = "%p" + sl + "build" + sl;
			string ValaFT_01_WD = "%p" + sl + "build" + sl;
			string EX_01_CM = "gdb " + "%p" + sl + "build" + sl + "src" + sl + name;
			string ValaFT_02_WD = "%p" + sl + "build" + sl;
			
			return @"[editor]
line_wrapping=false
line_break_column=72
auto_continue_multiline=true

[file_prefs]
final_new_line=true
ensure_convert_new_lines=false
strip_trailing_spaces=false
replace_tabs=false

[indentation]
indent_width=4
indent_type=1
indent_hard_tab_width=8
detect_indent=false
detect_indent_width=false
indent_mode=2

[project]
name=$name
base_path=$path
description=
file_patterns=

[long line marker]
long_line_behaviour=1
long_line_column=72

[build-menu]
EX_00_LB=_Execute
EX_00_CM=$EX_00_CM
EX_00_WD=
ValaFT_00_LB=_Compile (run meson)
ValaFT_00_CM=meson ..
ValaFT_00_WD=$ValaFT_00_WD
ValaFT_01_LB=_Build (run ninja)
ValaFT_01_CM=ninja
ValaFT_01_WD=$ValaFT_01_WD
filetypes=Vala;
EX_01_LB=_Debug
EX_01_CM=$EX_01_CM
EX_01_WD=
ValaFT_02_LB=Clean (_ninja clean)
ValaFT_02_CM=ninja clean
ValaFT_02_WD=$ValaFT_02_WD

[prjorg]
source_patterns=*.c;*.C;*.cpp;*.cxx;*.c++;*.cc;*.m;
header_patterns=*.h;*.H;*.hpp;*.hxx;*.h++;*.hh;
ignored_dirs_patterns=.*;CVS;
ignored_file_patterns=*.o;*.obj;*.a;*.lib;*.so;*.dll;*.lo;*.la;*.class;*.jar;*.pyc;*.mo;*.gmo;
generate_tag_prefs=0
external_dirs=

[files]

";
		}
		
		public static string get_po_POTFILES_file(string Name){
			return @"data/org.gnome.$Name.desktop.in
data/org.gnome.$Name.appdata.xml.in
data/org.gnome.$Name.gschema.xml
src/window.ui
src/main.vala
src/window.vala		
";
		}
		
		public static string get_data_meson_file(string Name){
			
			return @"desktop_file = i18n.merge_file(
  input: 'org.gnome.$Name.desktop.in',
  output: 'org.gnome.$Name.desktop',
  type: 'desktop',
  po_dir: '../po',
  install: true,
  install_dir: join_paths(get_option('datadir'), 'applications')
)

desktop_utils = find_program('desktop-file-validate', required: false)
if desktop_utils.found()
  test('Validate desktop file', desktop_utils,
    args: [desktop_file]
  )
endif

appstream_file = i18n.merge_file(
  input: 'org.gnome.$Name.appdata.xml.in',
  output: 'org.gnome.$Name.appdata.xml',
  po_dir: '../po',
  install: true,
  install_dir: join_paths(get_option('datadir'), 'appdata')
)

appstream_util = find_program('appstream-util', required: false)
if appstream_util.found()
  test('Validate appstream file', appstream_util,
    args: ['validate', appstream_file]
  )
endif

install_data('org.gnome.$Name.gschema.xml',
  install_dir: join_paths(get_option('datadir'), 'glib-2.0/schemas')
)

compile_schemas = find_program('glib-compile-schemas', required: false)
if compile_schemas.found()
  test('Validate schema file', compile_schemas,
    args: ['--strict', '--dry-run', meson.current_source_dir()]
  )
endif

";
		}
		
		public static string get_data_appdata_xml_in_file(){
			return """<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
	<id>org.gnome.%s.desktop</id>
	<metadata_license>CC0-1.0</metadata_license>
	<project_license>LGPL-3.0-or-later</project_license>
	<description>
	</description>
</component>
""";
		}
		
		public static string get_data_desktop_in_file(string name){
			return @"[Desktop Entry]
Name=$name
Exec=$name
Terminal=false
Type=Application
Categories=GTK;
StartupNotify=true
";
		}
		public static string get_data_gschema_xml_file(){
			return """<?xml version="1.0" encoding="UTF-8"?>
<schemalist gettext-domain="%s">
	<schema id="org.gnome.%s" path="/org/gnome/%s/">
	</schema>
</schemalist>
""";
		}
		
		public static string get_build_aux_postinstall_file(){
			return """#!/usr/bin/env python3

from os import environ, path
from subprocess import call

prefix = environ.get('MESON_INSTALL_PREFIX', '/usr/local')
datadir = path.join(prefix, 'share')
destdir = environ.get('DESTDIR', '')

# Package managers set this so we don't need to run
if not destdir:
    print('Updating icon cache...')
    call(['gtk-update-icon-cache', '-qtf', path.join(datadir, 'icons', 'hicolor')])

    print('Updating desktop database...')
    call(['update-desktop-database', '-q', path.join(datadir, 'applications')])

    print('Compiling GSettings schemas...')
    call(['glib-compile-schemas', path.join(datadir, 'glib-2.0', 'schemas')])
			
""";
		}
	}
}
