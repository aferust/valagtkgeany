/* window.vala
 *
 * Copyright 2018 Ferhat Kurtulmuş <aferust@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/* 
 * run in app's bin folder to copy all required dll's to there.
 * 
 * ldd valagtkgeany.exe | grep '\/mingw.*\.dll' -o | xargs -I{} cp "{}" .
 * 
 */
 
namespace Valagtkgeany {
	string slash;
	
	string gtkver;
	string giover;
	
	[GtkTemplate (ui = "/org/gnome/Valagtkgeany/window.ui")]
	public class Window : Gtk.ApplicationWindow {
		[GtkChild]
		Gtk.Entry entry1;
		
		[GtkChild]
		Gtk.Entry entry2;
		
		[GtkChild]
		Gtk.Entry entry3;
		
		[GtkChild]
		Gtk.Label label1;
		
		[GtkChild]
		Gtk.Button but1;
		
		[GtkChild]
		Gtk.Button but2;
		
		[GtkChild]
		Gtk.TextView textView1;
		
		Gtk.FileChooserDialog s_dialog;
		GLib.File? file;
		
		Creator worker;
		
		public Window (Gtk.Application app) {
			Object (application: app);
			
			this.border_width = 5;
			
			slash = Path.DIR_SEPARATOR_S;
			file = GLib.File.new_for_path(Environment.get_home_dir() + slash + "Desktop");
			
			entry1.set_text("helloworld");
			entry2.set_text("3.20"); gtkver = entry2.get_text();
			entry3.set_text("2.48.2"); giover = entry3.get_text();
			
			label1.set_text(file.get_path() + slash + entry1.get_text().strip());
			entry1.changed.connect(()=>{
				label1.set_text(file.get_path() + slash + entry1.get_text().strip());
			});
			
			
			but1.clicked.connect (() => {
				s_dialog = new Gtk.FileChooserDialog ("Set a root folder for the project",
															 this as Gtk.Window,
															 Gtk.FileChooserAction.SELECT_FOLDER,
															 "_Cancel",
															 Gtk.ResponseType.CANCEL,
															 "_Select",
															 Gtk.ResponseType.ACCEPT);
				
				s_dialog.set_modal (true);
				if (file != null) {
					try {
						(s_dialog as Gtk.FileChooser).set_file (file);
					}
					catch (GLib.Error error) {
						print ("%s\n", error.message);
					}
				}
				s_dialog.response.connect (this.set_as_response_cb);
				s_dialog.show ();
			});
			
			but2.clicked.connect (() => {
				GLib.File fl;
				if(is_valid_file_name(entry1.get_text().strip())){
					try {
						fl = GLib.File.new_for_path(file.get_path() + slash + entry1.get_text().strip());
						fl.make_directory ();
						this.append_str("\nBase folder created.");
						
						gtkver = entry2.get_text();
						giover = entry3.get_text();
						
						worker = new Creator(fl.get_path(), entry1.get_text().strip());
						
						worker.make_root_folder();
						this.append_str(worker.make_src_folder());
						this.append_str(worker.make_po_folder());
						this.append_str(worker.make_data_folder());
						this.append_str(worker.make_build_aux_folder());
						this.append_str(worker.make_build_folder());
						this.append_str(worker.make_win_res_folder());
					} catch (Error e) {
						this.append_str("\n" + e.message);
					}					
				}else{
					this.append_str("\nPlease provide a valid file name!");
				}


			});
			
		}
		
		public void set_as_response_cb(Gtk.Dialog dialog, int response_id){
			s_dialog = dialog as Gtk.FileChooserDialog;
			s_dialog.set_transient_for(this);
			switch (response_id) {
				case Gtk.ResponseType.ACCEPT:
					file = s_dialog.get_file();
					label1.set_text(file.get_path() + slash + entry1.get_text().strip());
					break;
				case Gtk.ResponseType.CANCEL:
					break;
				case Gtk.ResponseType.CLOSE:
					break;
				default:
					break;
			}
			
			s_dialog.destroy();			
		}
		
		public void append_str(string str){
			Gtk.TextIter myIter;
			int bytes = str.length;
			this.textView1.buffer.get_end_iter(out myIter);
			this.textView1.buffer.insert(ref myIter, str, bytes);
		}
		
		public bool is_valid_file_name(string str){
			if(str == "") return false;
			if(str == null) return false;
			for (int i = 0; i < str.char_count (); i++) {
				string chr = str.get_char (str.index_of_nth_char (i)).to_string ();
				if(Uri.RESERVED_CHARS_ALLOWED_IN_PATH.index_of(chr) != -1) return false;
				if(chr == " ") return false;
			}
			return true;
		}
		
	}
}
