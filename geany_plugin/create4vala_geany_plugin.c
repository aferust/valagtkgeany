#include <geanyplugin.h>
#include <gtk/gtk.h>

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
	void *window;

	window = valagtkgeany_window_new (app);
	//gtk_window_set_title (GTK_WINDOW (window), "Window");
	//gtk_window_set_default_size (GTK_WINDOW (window), 200, 400);
	gtk_widget_show_all (window);
}

static void item_activate_cb(GtkMenuItem *menuitem, gpointer user_data)
{
    //dialogs_show_msgbox(GTK_MESSAGE_INFO, "Hello World");
	GtkApplication *app;
	int status;

	app = gtk_application_new ("org.gnome.Valagtkgeany", G_APPLICATION_FLAGS_NONE);
	g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
	status = g_application_run (G_APPLICATION (app), 0, 0);
	g_object_unref (app);
}

static gboolean hello_init(GeanyPlugin *plugin, gpointer pdata)
{
    GtkWidget *main_menu_item;
    // Create a new menu item and show it
    main_menu_item = gtk_menu_item_new_with_mnemonic("Create Vala/Gtk+/meson project");
    gtk_widget_show(main_menu_item);
    gtk_container_add(GTK_CONTAINER(plugin->geany_data->main_widgets->tools_menu),
            main_menu_item);
    g_signal_connect(main_menu_item, "activate",
            G_CALLBACK(item_activate_cb), NULL);
    geany_plugin_set_data(plugin, main_menu_item, NULL);
    return TRUE;
}

static void hello_cleanup(GeanyPlugin *plugin, gpointer pdata)
{
    GtkWidget *main_menu_item = (GtkWidget *) pdata;
    gtk_widget_destroy(main_menu_item);
}

G_MODULE_EXPORT
void geany_load_module(GeanyPlugin *plugin)
{
    plugin->info->name = "Vala-Gtk+-meson";
    plugin->info->description = "Creates a vala/Gtk+ helloworld project using meson build system";
    plugin->info->version = "1.0";
    plugin->info->author = "Ferhat Kurtulmus <aferust@gmail.com>";
    plugin->funcs->init = hello_init;
    plugin->funcs->cleanup = hello_cleanup;
    GEANY_PLUGIN_REGISTER(plugin, 225);
}
